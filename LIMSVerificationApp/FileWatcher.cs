﻿using System;
using System.Threading;
using System.IO;
using System.Drawing;

namespace LIMSVerificationApp
{
    public class FileWatcher
    {
        private string DestinationDirectory;
        private string SourceDirectory;
        private string VerifyDirectory;
        private int Timeout;
        
        // this text is contained either in the PCR plate barcode (for SB rack-to-plate)
        // or this text is contained in the pooled rack barcode (for P discrete-to-pooled)
        private string TestBarcode = "TEST";

        public event EventHandler<string> WriteText;
        public event EventHandler<bool> ShowProgressBar;
        public event EventHandler<Color> SetBackgroundColor;

        // current directory: /Barcode Files/LIMSVerification/App/
        // parent directory: /Barcode Files/LIMSVerification/
        // grand parent directory: /Barcode Files/

        // source directoy: /Barcode Files/LIMSVerification/App/Test Files/ [current + /Test Files/]
        // destination directory: /Barcode Files/ [grand parent]
        // verify directory: /Barcode Files/LIMSVerification/ [parent]

        // constructor gives values to Directory fiels and Timeout 
        public FileWatcher()
        {
            Timeout = 120;

            string currentDirectory = Directory.GetCurrentDirectory().ToString();
            string parentDirectory = Directory.GetParent(currentDirectory).ToString();
            string grandParentDirectory = Directory.GetParent(parentDirectory).ToString();

            VerifyDirectory = parentDirectory;
            SourceDirectory = Path.Combine(currentDirectory, "Test Files");
            DestinationDirectory = grandParentDirectory;
        }

        // main workflow- files copied to /Barcode Files/ and then /LIMSVerification/ monitored for .verify file
        public void DoWork()
        {
            
            if (CheckVerifyDirectory() && CheckSourceDirectory() && CheckDestinationDirectory())
            {
                CopyFiles();
                CheckForVerification();
            }
        }

        // EventHandler to write text to label1
        protected virtual void OnWriteText(string text)
        {
            if (WriteText != null)
                WriteText(this, text);
        }

        // EventHandler to show/hide progressBar1
        protected virtual void OnShowProgressBar(bool val)
        {
            if (ShowProgressBar != null)
                ShowProgressBar(this, val);
        }

        // EventHandler to set background color of lablel1
        protected virtual void OnSetBackgroundColor(Color color)
        {
            if (SetBackgroundColor != null)
                SetBackgroundColor(this, color);
        }


        /* clears VerificationDirectory of all files
         * returns TRUE if no exceptions thrown
         * returns FALSE if GetFiles throws an exception
         */
        private bool CheckVerifyDirectory()
        {
            try
            {
                string[] files = Directory.GetFiles(VerifyDirectory);
                foreach (string f in files)
                {
                    File.Delete(f);
                }
                OnWriteText($"files deleted from verify directory: {String.Join("\n", files)}");
                return true;
            }
            catch (Exception e)
            {
                OnWriteText(e.Message);
                OnSetBackgroundColor(Color.Red);
                return false;
            }
        }

        /* checks SourceDirectory to ensure it's not empty
         * returns TRUE if sourceDirectory contains any files
         * returns FALSE if sourceDirectory is empty or if GetFiles throws an exception
         */
        private bool CheckSourceDirectory()
        {

            string[] files = Directory.GetFiles(SourceDirectory);
            if(files.Length != 0)
            {
                OnWriteText($"{SourceDirectory} is not empty.");
                return true;
            }
            else
            {
                OnWriteText($"{SourceDirectory} is empty. Ensure it has the proper test files.");
                OnSetBackgroundColor(Color.Red);
                return false;
            }
        }

       /* checks destinationDirectory for any existing files
        * returns TRUE if destinationDirectory is empty
        * returns FALSE if destinationDirectory contains any files or if GetFiles throws an exception
        */
        private bool CheckDestinationDirectory()
        {
            try
            {
                string[] files = Directory.GetFiles(DestinationDirectory);
                if (files.Length == 0)
                {
                    OnWriteText("Destination folder empty");
                    return true;
                }
                else
                {
                    OnWriteText($"FILES FOUND IN: {DestinationDirectory} \n \n PLEASE CHECK IF FILES ARE NEEDED BY LIMS TEAM. \n IF NOT- DELETE FILES AND RE-START THE APP.");
                    OnSetBackgroundColor(Color.Red);
                    return false;
                }
            }
            catch (Exception e)
            {
                OnWriteText(e.Message);
                OnSetBackgroundColor(Color.Red);
                return false;
            }
        }

        // copies files from sourceDirectory to destinationDirectory 
        private void CopyFiles()
        {
            try
            {
                string[] files = Directory.GetFiles(SourceDirectory);
                foreach (string s in files)
                {
                    string fileName = Path.GetFileName(s);
                    string destFile = Path.Combine(DestinationDirectory, fileName);
                    File.Copy(s, destFile, true);
                }
                OnWriteText($"files copied into destination directory: {String.Join("\n", files)}");
            }
            catch (Exception e)
            {
                OnWriteText(e.Message);
                OnSetBackgroundColor(Color.Red);
            }
        }

        /* monitor /LIMSVerification/ directory
         * if a file appears, check if the name contains TestBarcode- if so, check the contents and display SUCCESS/FAILED
         * if no file appears before time, display FAILURE
         */
        private void CheckForVerification()
        {
            try
            {
                for (int i = 0; i < Timeout; i++)
                {
                    OnWriteText("checking verification folder...");
                    OnShowProgressBar(true);
                    string[] files = Directory.GetFiles(VerifyDirectory);
                    if (files.Length == 0)
                    {
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        foreach (string f in files)
                        {
                            string fName = f.Substring(VerifyDirectory.Length + 1);

                            // if Mirth drops a .verify file, LIMS communication was successful!
                            if (fName.Contains(TestBarcode))
                            {
                                OnWriteText("SUCCESSFUL LIMS COMMUNICATION");
                                OnShowProgressBar(false);
                                OnSetBackgroundColor(Color.Green);
                                return;
                            }
                            else
                            {
                                Thread.Sleep(1000);
                            }
                        }
                    }
                }
                OnWriteText($"FAILED LIMS COMMUNICATION: {Timeout}s timeout exceeded");
                OnShowProgressBar(false);
                OnSetBackgroundColor(Color.Red);
            }
            catch (Exception e)
            {
                OnWriteText($"FAILED LIMS COMMUNICATION: {e.Message}");
                OnShowProgressBar(false);
                OnSetBackgroundColor(Color.Red);
            }
        }
    }
}

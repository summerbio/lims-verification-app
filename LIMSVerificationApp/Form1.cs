﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;

namespace LIMSVerificationApp
{
    public partial class Form1 : Form
    {
        private delegate void WriteTextDelegate(string text);
        private delegate void HideProgressBarDelegrate(bool val);
        private delegate void SetBackgroundColorDelegate(Color color);

        public Form1()
        {
            InitializeComponent();
            FileWatcher fileWatcher = new FileWatcher();
            fileWatcher.WriteText += OnWriteText;
            fileWatcher.ShowProgressBar += OnShowProgressBar;
            fileWatcher.SetBackgroundColor += OnSetBackgroundColor;

            ShowProgressBar(false);

            Thread workerThread = new Thread(fileWatcher.DoWork);
            workerThread.IsBackground = true;
            workerThread.Start();
        }

        private void WriteSafeText(string text)
        {
            if (label1.InvokeRequired)
            {
                var d = new WriteTextDelegate(WriteSafeText);
                label1.Invoke(d, new object[] { text }) ;
            }
            else
            {
                label1.Text = text;
            }
        }

        private void ShowProgressBar(bool val)
        {
            if (progressBar1.InvokeRequired)
            {
                var d = new HideProgressBarDelegrate(ShowProgressBar);
                progressBar1.Invoke(d, new object[] {val});
            }
            else
            {
                progressBar1.Visible = val;
            }
        }

        private void SetBackgroundColor(Color color)
        {
            if (label1.InvokeRequired)
            {
                var d = new SetBackgroundColorDelegate(SetBackgroundColor);
                label1.Invoke(d, new object[] {color});
            }
            else
            {
                label1.BackColor = color;
            }
        }

        public void OnWriteText(object source, string text)
        {
            WriteSafeText(text);
        }

        public void OnShowProgressBar(object source, bool val)
        {
            ShowProgressBar(val); 
        }

        public void OnSetBackgroundColor(object source, Color color)
        {
            SetBackgroundColor(color);
        }
    }
}
